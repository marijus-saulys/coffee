## Info

To install required modules you have to run command:

### `npm i`

After that you can start app by running command in terminal from app root:

### `npm run dev`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Main sources are in here:

`src/app/modules/Simulator`

Main algorithm is commented in function `tick`

## UI

Queues are ordered from right to left, top to bottom, where the most top right is the first to leave the queue. The most right from lower row goes to upper row to the most left position. 

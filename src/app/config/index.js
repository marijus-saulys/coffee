const TIME_LIMITS = {
  TICK: 100,
  WORKING_PERIOD: 60 * 60 * 1000,
  COFFEE_MAKING_PERIOD: 5 * 60 * 1000,
  BUSY_GENERATION_PERIOD: 5 * 60 * 1000,
};

const SPEED_LIMITS = {
  MAX: 100,
  FAST_STEP: 10,
  SLOW_STEP: 1,
};

const DEFAULTS = {
  SPEED: 10,
  ENGINEER_COUNT: 50,
  BUSY_CHANCE: 0.1,
  BUSY_PERIOD: 5,
};

const MOODS = {
  working: ['dizzy', 'flushed', 'grin-tongue', 'grin', 'smile'],
  thirsty: 'frown',
};

export {
  TIME_LIMITS,
  SPEED_LIMITS,
  DEFAULTS,
  MOODS,
};

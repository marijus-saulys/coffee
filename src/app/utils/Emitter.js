import React from 'react';

export default (Component) => (
  class Emitter extends React.Component {

    handlers = {};

    subscribe = (handlers) => {
      this.handlers = {
        ...this.handlers,
        ...handlers
      };
    };

    emit = (event, options) => (e) => {
      if (!!this.handlers[event]) {
        this.handlers[event](e, options);
      }
    };

    render = () => (
      <Component {...this.props} emit={this.emit} subscribe={this.subscribe}/>
    );
  }
)

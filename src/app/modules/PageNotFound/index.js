import React from 'react';

const view = () => (
  <div className="container-fluid">
    <div className="row justify-content-center mt-4 align-items-baseline">
      <span className="text-large p-2">Page Not Found.</span>
    </div>
  </div>
);

export default view;

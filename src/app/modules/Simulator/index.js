import React from 'react';
import _ from 'lodash';

import View, { EVENTS } from './view';
import Emitter from '../../utils/Emitter';
import { SPEED_LIMITS, TIME_LIMITS, DEFAULTS, MOODS } from '../../config';

class Component extends React.Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.state = {
      elapsed: TIME_LIMITS.WORKING_PERIOD,
      lastBusyGenerationTime: TIME_LIMITS.WORKING_PERIOD,

      speed: DEFAULTS.SPEED,
      pause: true,

      engineerCount: DEFAULTS.ENGINEER_COUNT,
      busyChance: DEFAULTS.BUSY_CHANCE,
      busyPeriod: DEFAULTS.BUSY_PERIOD,

      engineers: [],
      queue: {
        working: [],
        standard: [],
        priority: [],
        coffeeMaking: null,
        busy: [],
      }
    };

    this.props.subscribe({
      [EVENTS.FAST_FORWARD]: this.handleSpeedChange(SPEED_LIMITS.FAST_STEP),
      [EVENTS.FORWARD]: this.handleSpeedChange(SPEED_LIMITS.SLOW_STEP),
      [EVENTS.PAUSE]: this.handlePause(true),
      [EVENTS.PLAY]: this.handlePause(false),
      [EVENTS.BACKWARD]: this.handleSpeedChange(-SPEED_LIMITS.SLOW_STEP),
      [EVENTS.FAST_BACKWARD]: this.handleSpeedChange(-SPEED_LIMITS.FAST_STEP),

      [EVENTS.CHANGE_ENGINEER_COUNT]: this.handleChangeEngineerCount,
      [EVENTS.CHANGE_BUSY_CHANCE]: this.handleChangeBusyChance,
      [EVENTS.CHANGE_BUSY_PERIOD]: this.handleChangeBusyPeriod,
    });
  }

  componentDidMount() {
    const engineers = this.generateEngineers();
    this.setState({
      engineers,
      queue: {
        ...this.state.queue,
        working: _.map(_.orderBy(engineers, 'workStartTime', 'asc'), 'id'),
      }
    });
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  generateEngineers = (fromIdx = 0, toIdx = this.state.engineerCount, randomTime = true) => {
    const engineers = [];
    for (let idx = fromIdx; idx < toIdx; idx++) {
      engineers.push({
        id: idx,
        workStartTime: randomTime ? Math.floor(Math.random() * TIME_LIMITS.WORKING_PERIOD) : this.state.elapsed,
        busyStartTime: null,
        mood: MOODS.working[Math.floor(Math.random() * MOODS.working.length)],
      });
    }

    return engineers;
  };

  // this is the main algorithm
  tick = () => {
    const elapsed = this.state.elapsed + this.state.speed * TIME_LIMITS.TICK;
    const engineers = [ ...this.state.engineers ];
    const queue = { ...this.state.queue };
    const busyPeriod = this.state.busyPeriod * 60 * 1000;

    // add new busy people
    let lastBusyGenerationTime = this.state.lastBusyGenerationTime;
    if (elapsed - lastBusyGenerationTime > TIME_LIMITS.BUSY_GENERATION_PERIOD) {
      lastBusyGenerationTime = elapsed;
      _.each(engineers, engineer => {
        if (!engineer.busyStartTime && (Math.random() < this.state.busyChance)) {
          engineer.busyStartTime = elapsed;
          queue.busy.push(engineer.id);

          // if he was in the standard queue, put him in priority at the end
          const index = queue.standard.indexOf(engineer.id);
          if (index >= 0) {
            queue.standard.splice(index, 1);
            queue.priority.push(engineer.id);
          }
        }
      });
    }

    // let tired busy people rest
    if (queue.busy.length && (elapsed - engineers[queue.busy[0]].busyStartTime > busyPeriod)) {
      const tired = queue.busy.shift();
      engineers[tired].busyStartTime = null;

      // kick him to the end of the queue.
      const index = queue.priority.indexOf(tired);
      if (index >= 0) {
        queue.priority.splice(index, 1);
        queue.standard.push(tired);
      }
    }

    // check if there's a thirsty engineer already
    if (queue.working.length && (elapsed - engineers[queue.working[0]].workStartTime > TIME_LIMITS.WORKING_PERIOD)) {
      const thirsty = queue.working.shift();
      engineers[thirsty].workStartTime = null;

      // put him into according queue
      queue[engineers[thirsty].busyStartTime ? 'priority' : 'standard'].push(thirsty);
    }

    // check if there's a slacker at coffee machine
    if (queue.coffeeMaking && (elapsed - queue.coffeeMaking.startTime > TIME_LIMITS.COFFEE_MAKING_PERIOD)) {
      // get him back to work
      queue.working.push(queue.coffeeMaking.id);
      engineers[queue.coffeeMaking.id].workStartTime = elapsed;
      queue.coffeeMaking = null;
    }

    // if there's no one making coffee and there's someone waiting for it
    if (!queue.coffeeMaking && (queue.priority.length || queue.standard.length)) {
      // let them get some coffee (first priority queue, then standard)
      queue.coffeeMaking = {
        id: queue.priority.length ? queue.priority.shift() : queue.standard.shift(),
        startTime: elapsed,
      };
    }

    // refresh state
    this.setState({
      lastBusyGenerationTime,
      elapsed,
      engineers,
      queue,
    });
  };

  handleSpeedChange = (moment) => () => {
    const max = this.state.speed + moment > SPEED_LIMITS.MAX;
    const min = this.state.speed + moment < 1;
    this.setState({ speed: (max) ? SPEED_LIMITS.MAX : (min ? 1 : this.state.speed + moment) });
  };

  handlePause = (pause) => () => {
    if (pause) {
      clearInterval(this.timer);
    } else {
      this.timer = setInterval(this.tick, TIME_LIMITS.TICK);
    }
    this.setState({ pause });
  };

  handleChangeEngineerCount = ({ target }) => {
    const engineerCount = parseInt(target.value);
    const queue = { ...this.state.queue };
    let engineers = [ ...this.state.engineers ];
    if (engineerCount > this.state.engineerCount) {
      const newEngineers = this.generateEngineers(this.state.engineerCount, engineerCount, false);
      engineers = [ ...engineers, ...newEngineers ];
      queue.working = [ ...queue.working, ..._.map(newEngineers, 'id') ];
    } else {
      engineers = _.filter(engineers, engineer => engineer.id < engineerCount);
      queue.working = _.filter(queue.working, id => id < engineerCount);
      queue.standard = _.filter(queue.standard, id => id < engineerCount);
      queue.priority = _.filter(queue.priority, id => id < engineerCount);
      queue.busy = _.filter(queue.busy, id => id < engineerCount);
      queue.coffeeMaking = (queue.coffeeMaking && (queue.coffeeMaking.id < engineerCount)) ? queue.coffeeMaking : null;
    }
    console.log(engineers);
    this.setState({
      engineerCount,
      engineers,
      queue,
    });
  };

  handleChangeBusyChance = ({ target }) => {
    this.setState({ busyChance: target.value });
  };

  handleChangeBusyPeriod = ({ target }) => {
    this.setState({ busyPeriod: target.value });
  };

  render() {
    return (
      <View {...this.state} {...this.props}/>
    );
  }
}

export default Emitter(Component);

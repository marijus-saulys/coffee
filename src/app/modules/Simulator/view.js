import React from 'react';

import Params, { EVENTS as PARAM_EVENTS } from './components/params';
import Queue from './components/queue';
import Working from './components/working';

export const EVENTS = {
  ...PARAM_EVENTS
};

const view = ({ ...props }) => (
  <div className="container">
    <div className="row border-bottom mb-1">
      <h2>Espresso machine simulator</h2>
    </div>
    <div className="row">
      <div className="col-md-4">
        <Params {...props}/>
      </div>
      <div className="col-md-8">
        <Queue {...props}/>
      </div>
    </div>
    <div className="row">
      <Working {...props}/>
    </div>
  </div>
);

export default view;

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const EVENTS = {
  FAST_FORWARD: 'FAST_FORWARD',
  FORWARD: 'FORWARD',
  PAUSE: 'PAUSE',
  PLAY: 'PLAY',
  BACKWARD: 'BACKWARD',
  FAST_BACKWARD: 'FAST_BACKWARD',

  CHANGE_ENGINEER_COUNT: 'CHANGE_ENGINEER_COUNT',
  CHANGE_BUSY_CHANCE: 'CHANGE_BUSY_CHANCE',
  CHANGE_BUSY_PERIOD: 'CHANGE_BUSY_PERIOD',
};

const view = ({ speed, pause, engineerCount, busyChance, busyPeriod, emit }) => (
  <div className="container mt-3 border-right border-bottom">
    <div className="row border-bottom mb-2">
      <h5>Control Center (Speed: x{speed}{pause ? ' / paused' : ''})</h5>
    </div>
    <div className="container-fluid p-3 mb-3 border-bottom">
      <div className="row">
        <div className="col-md-2">
          <button type="button" className="btn btn-info btn-sm" onClick={emit(EVENTS.FAST_BACKWARD)}>
            <FontAwesomeIcon icon="fast-backward"/>
          </button>
        </div>
        <div className="col-md-2">
          <button type="button" className="btn btn-info btn-sm" onClick={emit(EVENTS.BACKWARD)}>
            <FontAwesomeIcon icon="backward"/>
          </button>
        </div>
        <div className="col-md-2">
          <button type="button" className={`btn btn-sm ${pause ? 'btn-success' : 'btn-info'}`}
                  onClick={emit(EVENTS.PAUSE)}>
            <FontAwesomeIcon icon="pause-circle"/>
          </button>
        </div>
        <div className="col-md-2">
          <button type="button" className={`btn btn-sm ${!pause ? 'btn-success' : 'btn-info'}`}
                  onClick={emit(EVENTS.PLAY)}>
            <FontAwesomeIcon icon="play-circle"/>
          </button>
        </div>
        <div className="col-md-2">
          <button type="button" className="btn btn-info btn-sm" onClick={emit(EVENTS.FORWARD)}>
            <FontAwesomeIcon icon="forward"/>
          </button>
        </div>
        <div className="col-md-2">
          <button type="button" className="btn btn-info btn-sm" onClick={emit(EVENTS.FAST_FORWARD)}>
            <FontAwesomeIcon icon="fast-forward"/>
          </button>
        </div>
      </div>
    </div>
    <div className="row form-group">
      <div className="container-fluid">
        <label>Number of Engineers</label>
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <FontAwesomeIcon icon="meh-rolling-eyes"/>
            </span>
          </div>
          <select className="custom-select" value={engineerCount} onChange={emit(EVENTS.CHANGE_ENGINEER_COUNT)}>
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
      </div>
    </div>
    <div className="row form-group">
      <div className="container-fluid">
        <label>Chance of becoming Super-Busy</label>
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <FontAwesomeIcon icon="dice"/>
            </span>
          </div>
          <select className="custom-select" value={busyChance} onChange={emit(EVENTS.CHANGE_BUSY_CHANCE)}>
            <option value="0.05">5%</option>
            <option value="0.1">10%</option>
            <option value="0.2">20%</option>
            <option value="0.3">30%</option>
            <option value="0.4">40%</option>
            <option value="0.5">50%</option>
            <option value="0.6">60%</option>
            <option value="0.7">70%</option>
            <option value="0.8">80%</option>
            <option value="0.9">90%</option>
          </select>
        </div>
        <small className="form-text text-muted">Recalculates after each coffee</small>
      </div>
    </div>
    <div className="row form-group">
      <div className="container-fluid">
        <label>Time limit of Super-Busy</label>
        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">
              <FontAwesomeIcon icon="stopwatch"/>
            </span>
          </div>
          <select className="custom-select" value={busyPeriod} onChange={emit(EVENTS.CHANGE_BUSY_PERIOD)}>
            <option value="1">1 min</option>
            <option value="5">5 min</option>
            <option value="15">15 min</option>
            <option value="30">30 min</option>
            <option value="60">1 hour</option>
          </select>
        </div>
      </div>
    </div>
  </div>
);

export default view;

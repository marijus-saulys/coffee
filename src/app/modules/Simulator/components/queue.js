import React from 'react';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Engineer from './engineer';

const view = ({ engineers, queue, elapsed, busyPeriod }) => (
  <div className="container mt-3">
    <div className="row border-bottom mb-3">
      <h5>Queue</h5>
    </div>
    <div className="row">
      <div className="col-md-9 justify-content-center border-right">
        <div className="row justify-content-end border-bottom p-1">
          <ul className="no-style-list">
            {
              _.map(queue.priority, id => (
                <Engineer key={id} data={engineers[id]} elapsed={elapsed} waiting={true} busyPeriod={busyPeriod}/>
              ))
            }
          </ul>
        </div>
        <div className="row justify-content-end p-1">
          <ul className="no-style-list">
            {
              _.map(queue.standard, id => (
                <Engineer key={id} data={engineers[id]} elapsed={elapsed} waiting={true} busyPeriod={busyPeriod}/>
              ))
            }
          </ul>
        </div>
      </div>
      <div className="col-md-3">
        <div className="row justify-content-center"><FontAwesomeIcon icon="coffee" className="fa-3x"/></div>
        <div className="row justify-content-center">
          <ul className="no-style-list">
            {
              queue.coffeeMaking && (
                <Engineer data={engineers[queue.coffeeMaking.id]} elapsed={elapsed}
                          coffeeMaking={queue.coffeeMaking} busyPeriod={busyPeriod}
                />
              )
            }
          </ul>
        </div>
      </div>
    </div>
  </div>
);

export default view;

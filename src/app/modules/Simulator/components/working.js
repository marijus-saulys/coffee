import React from 'react';
import _ from 'lodash';

import Engineer from './engineer';

const view = ({ engineers, queue, elapsed, busyPeriod }) => (
  <div className="container mt-3">
    <div className="row border-bottom mb-3">
      <h5>Working</h5>
    </div>
    <div className="row">
      <div className="col justify-content-center">
        <ul className="no-style-list">
          {
            _.map(queue.working, id => (
              <Engineer key={id} data={engineers[id]} elapsed={elapsed} busyPeriod={busyPeriod}/>
            ))
          }
        </ul>
      </div>
    </div>
  </div>
);

export default view;

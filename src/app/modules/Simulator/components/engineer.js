import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { TIME_LIMITS, MOODS } from '../../../config';

const view = ({ data, elapsed, waiting = false, coffeeMaking = null, busyPeriod }) => {
  const busy = busyPeriod  * 60 * 1000;
  let timeLeft = !coffeeMaking
    ? (TIME_LIMITS.WORKING_PERIOD - (elapsed - data.workStartTime)) : (elapsed - coffeeMaking.startTime);
  timeLeft = (timeLeft > 0) ? timeLeft : 0;
  let busyLeft = data.busyStartTime ? (busy - (elapsed - data.busyStartTime)) : 0;
  busyLeft = (busyLeft > 0) ? busyLeft : 0;

  const timePercentageLeft = !coffeeMaking
    ? timeLeft / TIME_LIMITS.WORKING_PERIOD * 100 : timeLeft / TIME_LIMITS.COFFEE_MAKING_PERIOD * 100;
  const busyPercentageLeft = busyLeft / busy * 100;

  // progress bars were more fun, but not working as expected... :(
  return (
    <li key={data.id} className={data.busyStartTime ? 'busy' : ''}>
      <div className="progress text-center text-dark">
        <div style={{width: '100%', backgroundColor: `rgba(255, 69, 0, ${busyPercentageLeft / 100})`}}>
          <b>{`${Math.floor(busyLeft / 60 / 1000)}:${(busyLeft / 1000 % 60).toFixed(0)}`}</b>
        </div>
      </div>
      {/*<div className="progress" style={{height: '10px'}}>*/}
        {/*<div className="progress-bar progress-bar-striped bg-danger" role="progressbar" style={{width: `${busyLeft}%`}}/>*/}
      {/*</div>*/}
      <div>
        <span><b>#{data.id}</b></span>
        <span><FontAwesomeIcon icon={waiting ? MOODS.thirsty : data.mood}/></span>
      </div>
      <div className="progress text-center text-dark">
        <div style={{width: '100%', backgroundColor: `rgba(139, 69, 19, ${timePercentageLeft / 100})`}}>
          <b>{`${Math.floor(timeLeft / 60 / 1000)}:${(timeLeft / 1000 % 60).toFixed(0)}`}</b>
        </div>
      </div>
      {/*<div className="progress" style={{height: '10px'}}>*/}
        {/*<div className="progress-bar progress-bar-striped bg-info" role="progressbar" style={{width: `${timeLeft}%`}}/>*/}
      {/*</div>*/}
    </li>
  );
};

export default view;

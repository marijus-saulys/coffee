import React from 'react';
import { Switch, Route } from 'react-router-dom'

import './index.css';
import Simulator from './modules/Simulator';
import PageNotFound from './modules/PageNotFound';

class Component extends React.Component {
  render() {
    return (
      <div className="container-fluid bg-light pt-3 pb-3">
        <Switch>
          <Route exact path='/' component={Simulator}/>
          <Route component={PageNotFound}/>
        </Switch>
      </div>
    );
  }
}

export default Component;
